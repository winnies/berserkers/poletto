#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include <queue>
#include <functional>
#include <iostream>
#include <QMessageBox>

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// Fonctions pricipales ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

/* Retourne le point au milieu de p1 et p2 */
MyMesh::Point getMiddlePoint(MyMesh::Point p1, MyMesh::Point p2)
{
    return (p1 + p2)/2.f;
}

/* Contraction d'arrête */
bool MainWindow::collapseEdge(MyMesh* _mesh, int edgeID) {

    EdgeHandle eh = _mesh->edge_handle(edgeID);

    if(!eh.is_valid()) {
        qDebug("L'arrête à collapse n'existe pas !");
        return false;
    }

    auto heh = _mesh->halfedge_handle(eh, 1);

    VertexHandle from = _mesh->from_vertex_handle(heh);
    VertexHandle to = _mesh->to_vertex_handle(heh);

    if(!_mesh->is_collapse_ok(heh)) {
        //qDebug("Collapse is not ok !");
        return false;
    }

    MyMesh::Point middle = getMiddlePoint(_mesh->point(from), _mesh->point(to));

    auto checkNormals = [&](MyMesh::VertexHandle center, MyMesh::VertexHandle skip) {
        OpenMesh::Vec3f oldNorm(0, 0, 0);
        for(auto vv_it = _mesh->vv_iter(center); vv_it.is_valid();) {
            auto p1 = *vv_it;
            if(p1 == skip) {
                vv_it++;
                continue;
            }
            auto p2 = *++vv_it;
            if (!vv_it.is_valid()) {
                break;
            }
            if(p2 == skip) {
                vv_it++;
                continue;
            }
            auto v1 = _mesh->point(p1) - middle;
            auto v2 = _mesh->point(p2) - middle;
            auto nextNorm = (v1 % v2).normalized();
            if (oldNorm == Vec3f(0, 0, 0)) {
                oldNorm = nextNorm;
                continue;
            }
            auto scalarProduct = oldNorm | nextNorm;
            if(scalarProduct < 0){
                return false;
            }
            oldNorm = nextNorm;
        }
        return true;
    };

    if(ui->checkBox->isChecked() && (!checkNormals(from, to) || !checkNormals(to, from))) {
        return false;
    }

    _mesh->set_point(to, middle);
    _mesh->collapse(heh);

    for (auto vf_it = _mesh->vf_iter(to); vf_it.is_valid(); ++vf_it) {
        _mesh->update_normal(*vf_it);
    }

    MyMesh::EdgeHandle ehs[2];
    for (auto voh_it = _mesh->voh_iter(to); voh_it.is_valid(); ++voh_it) {
        ehs[0] = _mesh->edge_handle(*voh_it);
        ehs[1] = _mesh->edge_handle(_mesh->next_halfedge_handle(*voh_it));

        for (auto eh : ehs) {
            updateEdgeLength(_mesh, eh);
            updateEdgeFaceAngle(_mesh, eh);
        }
    }

    updateVertexFlatness(_mesh, to);
    for (auto vv_it = _mesh->vv_iter(to); vv_it.is_valid(); ++vv_it) {
        updateVertexFlatness(_mesh, *vv_it);
    }

    MyMesh::HalfedgeHandle hehs[3];
    for (auto voh_it = _mesh->voh_iter(to); voh_it.is_valid(); ++voh_it) {
        hehs[0] = *voh_it;
        hehs[1] = _mesh->next_halfedge_handle(hehs[0]);
        hehs[2] = _mesh->next_halfedge_handle(_mesh->opposite_halfedge_handle(hehs[1]));

        for (auto heh : hehs) {
            updateEdgeFlatness(_mesh, _mesh->edge_handle(heh));
        }
    }

    _mesh->garbage_collection();

    return true;
}

/* fonction pratique pour faire des tirages aléatoires */
int randInt(int low, int high){return qrand() % ((high + 1) - low) + low;}

/* Fonction principale (simplification du maillage*/
void MainWindow::decimation(MyMesh* _mesh, int percent, QString method)
{
    int nbCollapse = (_mesh->n_edges() * (100 - percent)) / 300;
    qDebug() << "Nombre d'arrêtes à contracter : " << nbCollapse;
    qDebug() << "Nombre d'arrêtes à supprimer : " << nbCollapse * 3;
    qDebug() << "Nombre d'arrêtes total : " << _mesh->n_edges();

    auto collapseEdges = [&](std::function<bool(MyMesh::EdgeHandle eh1, MyMesh::EdgeHandle eh2)> comp) {
        for (int i(0); i < nbCollapse; ++i) {
            std::priority_queue<MyMesh::EdgeHandle, std::vector<MyMesh::EdgeHandle>, decltype(comp)> ehq(_mesh->edges_begin(), _mesh->edges_end(), comp);
            while (!ehq.empty()) {
                auto eh = ehq.top();
                if(collapseEdge(_mesh, eh.idx())) {
                    break;
                }
                ehq.pop();
            }
        }
    };

    if(method == "Aléatoire")
    {
        qDebug() << "Simplification : " << method << " à " << percent << "%";

        for(int i(0); i < nbCollapse; ++i) {
            collapseEdge(_mesh, randInt(0, _mesh->n_edges()));
        }
    }
    else if(method == "Par taille")
    {
        qDebug() << "Simplification : " << method << " à " << percent << "%";

        collapseEdges([&](MyMesh::EdgeHandle eh1, MyMesh::EdgeHandle eh2) {
            return _mesh->data(eh1).length > _mesh->data(eh2).length;
        });
    }
    else if(method == "Par angle")
    {
        collapseEdges([&](MyMesh::EdgeHandle eh1, MyMesh::EdgeHandle eh2) {
            return _mesh->data(eh1).faceAngle > _mesh->data(eh2).faceAngle ;
        });
    }
    else if(method == "Par planéité")
    {
        collapseEdges([&](MyMesh::EdgeHandle eh1, MyMesh::EdgeHandle eh2) {
            return _mesh->data(eh1).flatness > _mesh->data(eh2).flatness ;
        });
    }
    else
    {
        qDebug() << "Méthode inconnue !!!";
    }
    qDebug() << "Nouveau nombre d'arrêtes total : " << _mesh->n_edges();
}

///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// Autres fonctions /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

/* (Re)calcule tous les critères de sélection pour chaque arrête du maillage */
void MainWindow::updateCrits(MyMesh *_mesh)
{
    _mesh->update_face_normals();

    for(auto e_it = _mesh->edges_begin(); e_it != _mesh->edges_end(); ++e_it) {
        updateEdgeLength(_mesh, *e_it);
        updateEdgeFaceAngle(_mesh, *e_it);
    }

    for (auto v_it = _mesh->vertices_begin(); v_it != _mesh->vertices_end(); ++v_it) {
        updateVertexFlatness(_mesh, *v_it);
    }

    for(auto e_it = _mesh->edges_begin(); e_it != _mesh->edges_end(); ++e_it) {
        updateEdgeFlatness(_mesh, *e_it);
    }
}

/* (Re)calcule la longueur de chaque arrête du maillage */
void MainWindow::updateEdgeLength(MyMesh *_mesh, MyMesh::EdgeHandle eh)
{
    _mesh->data(eh).length = _mesh->calc_edge_length(eh);
}

/* (Re)calcule l'angle entre les 2 surfaces de chaque arrête du maillage */
void MainWindow::updateEdgeFaceAngle(MyMesh *_mesh, MyMesh::EdgeHandle eh)
{
    auto& faceAngle = _mesh->data(eh).faceAngle;

    if(_mesh->is_boundary(eh)) {
        faceAngle = M_PIf32 * 2;
    } else {
        auto n1 = _mesh->normal(_mesh->face_handle(_mesh->halfedge_handle(eh, 0)));
        auto n2 = _mesh->normal(_mesh->face_handle(_mesh->halfedge_handle(eh, 1)));
        faceAngle = std::acos(std::min((n1 | n2) / (n1.norm() * n2.norm()), 1.f));
    }
}

/* (Re)calcule la planéité de chaque arrête du maillage */
void MainWindow::updateEdgeFlatness(MyMesh *_mesh, MyMesh::EdgeHandle eh)
{
    auto p1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(eh, 0));
    auto p2 = _mesh->to_vertex_handle(_mesh->halfedge_handle(eh, 1));
    _mesh->data(eh).flatness = _mesh->data(p1).flatness + _mesh->data(p2).flatness;
}

/* (Re)calcule la planéité autour d'un sommet */
void MainWindow::updateVertexFlatness(MyMesh *_mesh, MyMesh::VertexHandle vh)
{
    auto& flatness = _mesh->data(vh).flatness;

    flatness = 0.f;
    for(auto ve_it = _mesh->ve_iter(vh); ve_it.is_valid(); ++ve_it) {
        flatness += _mesh->data(*ve_it).faceAngle;
    }
}

/* Met en évidence l'arrête couramment sélectionnée */
void MainWindow::showEdgeSelection(MyMesh* _mesh)
{
    // on réinitialise les couleurs de tout le maillage
    resetAllColorsAndThickness(_mesh);

    EdgeHandle eh = _mesh->edge_handle(edgeSelection);
    if(eh.is_valid()) {
        _mesh->set_color(eh, MyMesh::Color(255, 0, 0));
        _mesh->data(eh).thickness = 2;
    }

    // on affiche le nouveau maillage
    displayMesh(_mesh);
}

/* permet d'initialiser les couleurs et les épaisseurs des élements du maillage */
void MainWindow::resetAllColorsAndThickness(MyMesh* _mesh)
{
    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        _mesh->data(*curVert).thickness = 1;
        _mesh->set_color(*curVert, MyMesh::Color(0, 0, 0));
    }

    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        _mesh->set_color(*curFace, MyMesh::Color(150, 150, 150));
    }

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        _mesh->data(*curEdge).thickness = 1;
        _mesh->set_color(*curEdge, MyMesh::Color(0, 0, 0));
    }
}

void MainWindow::updateEdgeSelectionIHM()
{
    QString infos = "";

    auto eh = mesh.edge_handle(edgeSelection);
    if (eh.is_valid()) {
        auto& d = mesh.data(eh);
        infos = infos + "Surface : " + QString::number(0) + "\n";
        infos = infos + "C1 : " + QString::number(d.length) + "\n";
        infos = infos + "C2 : " + QString::number(d.faceAngle) + "\n";
        infos = infos + "C3 : " + QString::number(d.flatness) + "\n";
    }

    ui->infoEdgeSelection->setPlainText(infos);

    ui->labelEdge->setText(QString::number(edgeSelection));

    // on montre la nouvelle sélection
    showEdgeSelection(&mesh);
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// IHM /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void MainWindow::on_pushButton_edgeMoins_clicked()
{
    // mise à jour de l'interface
    edgeSelection = edgeSelection - 1;
    updateEdgeSelectionIHM();
}

void MainWindow::on_pushButton_edgePlus_clicked()
{
    // mise à jour de l'interface
    edgeSelection = edgeSelection + 1;
    updateEdgeSelectionIHM();
}

void MainWindow::on_pushButton_delSelEdge_clicked()
{
    // on supprime l'arête d'indice edgeSelection
    collapseEdge(&mesh, edgeSelection);

    // on actualise la sélection
    showEdgeSelection(&mesh);
}

void MainWindow::on_pushButton_chargement_clicked()
{
    // fenêtre de sélection des fichiers
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)"));

    // chargement du fichier .obj dans la variable globale "mesh"
    OpenMesh::IO::read_mesh(mesh, fileName.toUtf8().constData());

    updateCrits(&mesh);

    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // on affiche le maillage
    displayMesh(&mesh);
}

void MainWindow::on_pushButton_decimate_clicked()
{
    decimation(&mesh, ui->horizontalSlider->value(), ui->comboBox->currentText());
    displayMesh(&mesh);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// OPENGL /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

/* charge un objet MyMesh dans l'environnement OpenGL */
void MainWindow::displayMesh(MyMesh* _mesh, DisplayMode mode)
{
    GLuint* triIndiceArray = new GLuint[_mesh->n_faces() * 3];
    GLfloat* triCols = new GLfloat[_mesh->n_faces() * 3 * 3];
    GLfloat* triVerts = new GLfloat[_mesh->n_faces() * 3 * 3];

    int i = 0;

    if(mode == DisplayMode::TemperatureMap)
    {
        QVector<float> values;
        for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
            values.append(fabs(_mesh->data(*curVert).value));
        qSort(values);

        float range = values.at(values.size()*0.8);

        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;

        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }

    if(mode == DisplayMode::Normal)
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }

    if(mode == DisplayMode::ColorShading)
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            triCols[3*i+0] = _mesh->data(*fvIt).faceShadingColor[0]; triCols[3*i+1] = _mesh->data(*fvIt).faceShadingColor[1]; triCols[3*i+2] = _mesh->data(*fvIt).faceShadingColor[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->data(*fvIt).faceShadingColor[0]; triCols[3*i+1] = _mesh->data(*fvIt).faceShadingColor[1]; triCols[3*i+2] = _mesh->data(*fvIt).faceShadingColor[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->data(*fvIt).faceShadingColor[0]; triCols[3*i+1] = _mesh->data(*fvIt).faceShadingColor[1]; triCols[3*i+2] = _mesh->data(*fvIt).faceShadingColor[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }


    ui->displayWidget->loadMesh(triVerts, triCols, _mesh->n_faces() * 3 * 3, triIndiceArray, _mesh->n_faces() * 3);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
    GLfloat* linesCols = new GLfloat[_mesh->n_edges() * 2 * 3];
    GLfloat* linesVerts = new GLfloat[_mesh->n_edges() * 2 * 3];

    i = 0;
    QHash<float, QList<int> > edgesIDbyThickness;
    for (MyMesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit)
    {
        float t = _mesh->data(*eit).thickness;
        if(t > 0)
        {
            if(!edgesIDbyThickness.contains(t))
                edgesIDbyThickness[t] = QList<int>();
            edgesIDbyThickness[t].append((*eit).idx());
        }
    }
    QHashIterator<float, QList<int> > it(edgesIDbyThickness);
    QList<QPair<float, int> > edgeSizes;
    while (it.hasNext())
    {
        it.next();

        for(int e = 0; e < it.value().size(); e++)
        {
            int eidx = it.value().at(e);

            MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh1)[0];
            linesVerts[3*i+1] = _mesh->point(vh1)[1];
            linesVerts[3*i+2] = _mesh->point(vh1)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;

            MyMesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh2)[0];
            linesVerts[3*i+1] = _mesh->point(vh2)[1];
            linesVerts[3*i+2] = _mesh->point(vh2)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;
        }
        edgeSizes.append(qMakePair(it.key(), it.value().size()));
    }

    ui->displayWidget->loadLines(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

    GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
    GLfloat* pointsCols = new GLfloat[_mesh->n_vertices() * 3];
    GLfloat* pointsVerts = new GLfloat[_mesh->n_vertices() * 3];

    i = 0;
    QHash<float, QList<int> > vertsIDbyThickness;
    for (MyMesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit)
    {
        float t = _mesh->data(*vit).thickness;
        if(t > 0)
        {
            if(!vertsIDbyThickness.contains(t))
                vertsIDbyThickness[t] = QList<int>();
            vertsIDbyThickness[t].append((*vit).idx());
        }
    }
    QHashIterator<float, QList<int> > vitt(vertsIDbyThickness);
    QList<QPair<float, int> > vertsSizes;

    while (vitt.hasNext())
    {
        vitt.next();

        for(int v = 0; v < vitt.value().size(); v++)
        {
            int vidx = vitt.value().at(v);

            pointsVerts[3*i+0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
            pointsVerts[3*i+1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
            pointsVerts[3*i+2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
            pointsCols[3*i+0] = _mesh->color(_mesh->vertex_handle(vidx))[0];
            pointsCols[3*i+1] = _mesh->color(_mesh->vertex_handle(vidx))[1];
            pointsCols[3*i+2] = _mesh->color(_mesh->vertex_handle(vidx))[2];
            pointsIndiceArray[i] = i;
            i++;
        }
        vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
    }

    ui->displayWidget->loadPoints(pointsVerts, pointsCols, i * 3, pointsIndiceArray, i, vertsSizes);

    delete[] pointsIndiceArray;
    delete[] pointsCols;
    delete[] pointsVerts;
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    edgeSelection = -1;

    ui->setupUi(this);

    mesh.request_vertex_status();
    mesh.request_edge_status();
    mesh.request_face_status();
}

MainWindow::~MainWindow()
{
    delete ui;
}

